import java.util.Scanner;

public class TicTacToe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        do {
            playGame(scanner);
        } while (doesUserWantsToPlayAgain(scanner));
    }

    public static boolean doesUserWantsToPlayAgain(Scanner scanner) {
        System.out.println("Would you like to play one more time? y/n");
        String playAgain = scanner.nextLine();
        return "y".equals(playAgain);
    }

    public static void playGame(Scanner scanner) {
        String currentSymbol = getFirstSymbol(scanner);
        String[][] board = createBoard();
        printBoard(board);

        while (!isGameOver(board)) {
            int[] move = getCorrectMove(scanner, board);
            board[move[0]][move[1]] = currentSymbol;
            printBoard(board);
            currentSymbol = getNextSymbol(currentSymbol);
        }
        printResult(currentSymbol, board);
    }

    public static void printResult(String currentSymbol, String[][] board) {
        if (isSomeoneAWinner(board)) {
            System.out.println("Won: " + getNextSymbol(currentSymbol));
        } else {
            System.out.println("Draw!");
        }
    }

    public static boolean isSomeoneAWinner(String[][] board) {
        boolean row1 = board[0][0].equals(board[0][1]) && board[0][0].equals(board[0][2]) && !board[0][0].equals(" ");
        boolean row2 = board[1][0].equals(board[1][1]) && board[1][0].equals(board[1][2]) && !board[1][0].equals(" ");
        boolean row3 = board[2][0].equals(board[2][1]) && board[2][0].equals(board[2][2]) && !board[2][0].equals(" ");

        boolean column1 = board[0][0].equals(board[1][0]) && board[0][0].equals(board[2][0]) && !board[0][0].equals(" ");
        boolean column2 = board[0][1].equals(board[1][1]) && board[0][1].equals(board[2][1]) && !board[0][1].equals(" ");
        boolean column3 = board[0][2].equals(board[1][2]) && board[0][2].equals(board[2][2]) && !board[0][2].equals(" ");

        boolean diagonal1 = board[0][0].equals(board[1][1]) && board[0][0].equals(board[2][2]) && !board[0][0].equals(" ");
        boolean diagonal2 = board[2][0].equals(board[1][1]) && board[2][0].equals(board[0][2]) && !board[2][0].equals(" ");

        return row1 || row2 || row3
                || column1 || column2 || column3
                || diagonal1 || diagonal2;
    }

    public static String getFirstSymbol(Scanner scanner) {
        System.out.println("Who is starting x/o?");
        return scanner.nextLine();
    }

    public static String getNextSymbol(String currentSymbol) {
        if (currentSymbol.equals("o")) {
            return "x";
        }
        return "o";
    }

    public static int[] getMove(Scanner scanner) {
        int[] move = new int[2];
        move[0] = scanner.nextInt();
        move[1] = scanner.nextInt();
        scanner.nextLine();

        return move;
    }

    public static int[] getCorrectMove(Scanner scanner, String[][] board) {
        System.out.println("Choose place for your sign:");
        int[] move;
        do {
            move = getMove(scanner);
        } while (!isCorrectMove(board, move));
        return move;
    }

    public static boolean isCorrectMove(String[][] board, int[] move) {
        int row = move[0];
        int column = move[1];
        return " ".equals(board[row][column]);
    }

    public static boolean isGameOver(String[][] board) {
        return areAllCellsTaken(board) || isSomeoneAWinner(board);
    }

    public static boolean areAllCellsTaken(String[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (" ".equals(board[i][j])) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String[][] createBoard() {
        String[][] board = new String[3][3];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = " ";
            }
        }
        return board;
    }

    public static void printBoard(String[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]);
                if (j < board[i].length - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i < board.length - 1) {
                System.out.println("------");
            }
        }
    }

    public static String boardToString(String[][] board) {
        StringBuilder boardString = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                boardString.append(board[i][j]).append("\n");
            }
        }
        return boardString.toString();
    }
}
