import java.util.Random;
import java.util.Scanner;

public class GuessingGameOnePlayer {
    public static void main(String[] args) {
        playGame();
    }

    private static void playGame() {
        Scanner scanner = new Scanner(System.in);

        int computerNumber = new Random().nextInt(20);

        int playerGuess;
        do {
            System.out.println("Type your number: ");
            playerGuess = scanner.nextInt();
            if (playerGuess > computerNumber) {
                System.out.println("Given number is too big!");
            } else if (playerGuess < computerNumber) {
                System.out.println("Given number is too little!");
            }
        } while (playerGuess != computerNumber);
        System.out.println("You won! The correct number was: " + computerNumber);
    }
}