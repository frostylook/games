package GuessingGameTwoPlayers;

import java.util.Random;

public class GuessingGame {
    Gamer g1;
    Gamer g2;
    Gamer g3;

    void startGame() {
        g1 = new Gamer();
        g2 = new Gamer();
        g3 = new Gamer();

        int guess1 = 0;
        int guess2 = 0;
        int guess3 = 0;

        boolean g1guessed = false;
        boolean g2guessed = false;
        boolean g3guessed = false;

        Random random = new Random();
        int WinningNumber = random.nextInt(10);
        System.out.println("I've chosen a number from 0 to 10...");

        while (true) {
            System.out.println("Gamer one is choosing a number:");
            g1.tryGuess();
            System.out.println("Gamer two is choosing a number:");
            g2.tryGuess();
            System.out.println("Gamer three is choosing a number:");
            g3.tryGuess();

            guess1 = g1.number;
            System.out.println("Gamer one chose " + guess1);

            guess2 = g2.number;
            System.out.println("Gamer two chose " + guess2);

            guess3 = g3.number;
            System.out.println("Gamer three chose " + guess3);

            if (guess1 == WinningNumber) {
                g1guessed = true;
            }
            if (guess2 == WinningNumber) {
                g2guessed = true;
            }
            if (guess3 == WinningNumber) {
                g3guessed = true;
            }

            if (g1guessed || g2guessed || g3guessed) {
                System.out.println();
                System.out.println("We have winner! The number was: " + WinningNumber);
                System.out.println();

                if (g1guessed == true) {
                    System.out.println("The winner is gamer one");
                }
                if (g2guessed == true) {
                    System.out.println("The winner is gamer two");
                }
                if (g3guessed == true) {
                    System.out.println("The winner is gamer three");
                }
                System.out.println();
                System.out.println("The end of this game.");
                break;
            } else {
                System.out.println("No one guessed. Let's try again!");
            }
        }
    }
}